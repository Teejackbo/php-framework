<?php

namespace App\Controllers;

use App\Models\Visit;
use App\Framework\Extensions\ViewExtension\View;

class PageController
{
    public function index(View $view)
    {
        Visit::Log();
        $visitsToday = count(Visit::Query()
            ->select('*')
            ->where('date', date('Ymd'))
            ->where('page_name', $_SERVER['REQUEST_URI'])
            ->fetch());
        $visitsLastWeek = sizeof(Visit::AllLastWeek());
        $visits = Visit::FindAllBy('page_name', $_SERVER['REQUEST_URI']);
        $browsers = Visit::GetBrowsers();
        $title = 'Page Stats';

        return $view->render('index', compact(
            'visitsToday',
            'visitsLastWeek',
            'visits',
            'browsers',
            'title'
        ));
    }

    public function about(View $view)
    {
        Visit::Log();
        $visitsToday = count(Visit::Query()
            ->select('*')
            ->where('date', date('Ymd'))
            ->where('page_name', $_SERVER['REQUEST_URI'])
            ->fetch());
        $visitsLastWeek = sizeof(Visit::AllLastWeek());
        $visits = Visit::FindAllBy('page_name', $_SERVER['REQUEST_URI']);
        $browsers = Visit::GetBrowsers();
        $title = 'About';

        return $view->render('about', compact(
            'visitsToday',
            'visitsLastWeek',
            'visits',
            'browsers',
            'title'
        ));
    }

    public function contact(View $view)
    {
        Visit::Log();
        $visitsToday = count(Visit::Query()
            ->select('*')
            ->where('date', date('Ymd'))
            ->where('page_name', $_SERVER['REQUEST_URI'])
            ->fetch());
        $visitsLastWeek = sizeof(Visit::AllLastWeek());
        $visits = Visit::FindAllBy('page_name', $_SERVER['REQUEST_URI']);
        $browsers = Visit::GetBrowsers();
        $title = 'Contact';

        return $view->render('contact', compact(
            'visitsToday',
            'visitsLastWeek',
            'visits',
            'browsers',
            'title'
        ));
    }

    public function other(View $view)
    {
        Visit::Log();
        $visitsToday = count(Visit::Query()
            ->select('*')
            ->where('date', date('Ymd'))
            ->where('page_name', $_SERVER['REQUEST_URI'])
            ->fetch());
        $visitsLastWeek = sizeof(Visit::AllLastWeek());
        $visits = Visit::FindAllBy('page_name', $_SERVER['REQUEST_URI']);
        $browsers = Visit::GetBrowsers();
        $title = 'Other Page';

        return $view->render('other', compact(
            'visitsToday',
            'visitsLastWeek',
            'visits',
            'browsers',
            'title'
        ));
    }
}
