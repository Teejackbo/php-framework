<?php require 'partials/header.partial.php' ?>
<body>
  <h1>Contact</h1>
  <p>Total number of visits to this page: <?= sizeof($visits) ?></p>
  <p>Total number of visits to this page today: <?= $visitsToday ?></p>
  <p>Total number of visits to this page in the last week: <?= $visitsLastWeek ?></p>
  <p>Total number of visits by Chrome: <?= $browsers['chrome'] ?></p>
  <p>Total number of visits by Firefox: <?= $browsers['firefox'] ?></p>
  <p>Total number of visits by Safari: <?= $browsers['safari'] ?></p>
  <p>Total number of visits by Internet Explorer: <?= $browsers['ie'] ?></p>
</body>
</html>
