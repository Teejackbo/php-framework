<?php

namespace App\Config;

use App\Framework\Extensions\HttpExtension\Route;

Route::Get('/', 'PageController@index');
Route::Get('/about', 'PageController@about');
Route::Get('/contact', 'PageController@contact');
Route::Get('/otherpage', 'PageController@other');
