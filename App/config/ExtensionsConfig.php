<?php

namespace App\Config;

use App\Framework\Extensions\EnvExtension\EnvExtensionBooter;
use App\Framework\Extensions\HttpExtension\HttpExtensionBooter;
use App\Framework\Extensions\JsonExtension\JsonExtensionBooter;
use App\Framework\Extensions\ViewExtension\ViewExtensionBooter;
use App\Framework\Extensions\EventsExtension\EventsExtensionBooter;
use App\Framework\Extensions\ServiceExtension\ServiceExtensionBooter;
use App\Framework\Extensions\SessionExtension\SessionExtensionBooter;
use App\Framework\Extensions\DatabaseExtension\DatabaseExtensionBooter;
use App\Framework\Extensions\MiddlewareExtension\MiddlewareExtensionBooter;
use App\Framework\Extensions\SanitizationExtension\SanitizationExtensionBooter;

class ExtensionsConfig
{
    public $booters = [
        EnvExtensionBooter::class,
        JsonExtensionBooter::class,
        SessionExtensionBooter::class,
        DatabaseExtensionBooter::class,
        EventsExtensionBooter::class,
        ServiceExtensionBooter::class,
        SanitizationExtensionBooter::class,
        ViewExtensionBooter::class,
        MiddlewareExtensionBooter::class,
        HttpExtensionBooter::class
    ];
}
