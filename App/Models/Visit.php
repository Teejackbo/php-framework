<?php

namespace App\Models;

use App\Framework\Extensions\DatabaseExtension\Model;

class Visit extends Model
{
    protected static $table_name = 'visits';
    public $id;
    public $date;
    public $page_name;
    public $browser;

    public static function Table()
    {
        return static::$table_name;
    }

    public static function GetBrowser()
    {
        if (preg_match('/MSIE/i', $_SERVER['HTTP_USER_AGENT'])) {
            return 'Internet Explorer';
        } elseif (preg_match('/Firefox/i', $_SERVER['HTTP_USER_AGENT'])) {
            return 'Firefox';
        } elseif (preg_match('/Chrome/i', $_SERVER['HTTP_USER_AGENT'])) {
            return 'Chrome';
        } elseif (preg_match('/Safari/i', $_SERVER['HTTP_USER_AGENT'])) {
            return 'Safari';
        } elseif (preg_match('/Opera/i', $_SERVER['HTTP_USER_AGENT'])) {
            return 'Opera';
        } elseif (preg_match('/MSIE/i', $_SERVER['HTTP_USER_AGENT'])) {
            return 'Internet Explorer';
        } else {
            return 'Other';
        }
    }

    public static function Log()
    {
        $visit = new static;
        $visit->date = date('Ymd');
        $visit->page_name = $_SERVER['REQUEST_URI'];
        $visit->browser = static::GetBrowser();
        $visit->save();
    }

    public static function AllLastWeek()
    {
        return static::Query()
            ->select('*')
            ->where('date', '>', date('Ymd', strtotime(date('Ymd')) - 86400 * 6))
            ->where('page_name', $_SERVER['REQUEST_URI'])
            ->fetch();
    }

    public static function GetBrowsers()
    {
        $records = static::Query()
            ->select('*')
            ->where('page_name', $_SERVER['REQUEST_URI'])
            ->fetch();
        $chrome = 0;
        $firefox = 0;
        $safari = 0;
        $opera = 0;
        $ie = 0;

        foreach ($records as $record) {
            if ($record->browser == 'Chrome') {
                $chrome += 1;
            }
            if ($record->browser == 'Firefox') {
                $firefox += 1;
            }
            if ($record->browser == 'Safari') {
                $safari += 1;
            }
            if ($record->browser == 'Opera') {
                $opera += 1;
            }
            if ($record->browser == 'Internet Explorer') {
                $ie += 1;
            }
        }

        return [
            'chrome' => $chrome,
            'firefox' => $firefox,
            'safari' => $safari,
            'opera' => $opera,
            'ie' => $ie
        ];
    }
}
