<?php

// Server entry point. Used to route all requests to core/bootstrap.php
// Start server with `php -S localhost:8080 server.php`
require_once 'core/bootstrap.php';
