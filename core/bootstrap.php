<?php

namespace App\Framework\Core;

// Require files for autoloading and helper functions.
require_once 'vendor/autoload.php';
require_once 'functions.php';

// Attempts to run the application. If an error is caught, it is displayed within the browser.
try {
    $app = new \App\Framework\Core\Application;
    $app->new(\App\Config\ExtensionsConfig::class);
    $app->new(\App\Framework\Core\ExtensionBooter::class)->bootExtensions();
} catch (\Exception $e) {
    $message = $e->getMessage();
    $file = explode('/', $e->getFile());
    $line = $e->getLine();
    $trace = $e->getTrace();
    require_once __DIR__ . '/error_view.php';
}
