<?php

namespace App\Framework\Core\System;

/**
 * Provides information about the user's system.
 */
class System
{
    /**
     * Returns the root path of the application.
     */
    public static function GetRoot(): string
    {
        return $_SERVER['DOCUMENT_ROOT'];
    }
}
