<?php

namespace App\Framework\Core;

use App\Framework\Core\Interfaces\IExtension;

/**
 * Provides a template for an Extension, and makes the app accessible to the extension booter.
 */
abstract class Extension implements IExtension
{
    /**
     * The classes to be automatically injected into the DI container.
     *
     * @var array
     */
    public $classes;

    /**
     * An array of dependencies for that extension.
     * 
     * @var array
     */
    public $dependencies;

    /**
     * An array of config classes for that extension.
     */
    public $configs;

    /**
     * The created application.
     *
     * @var \App\Framework\Core\Application
     */
    public $app;

    /**
     * Returns a new Extension, with the app property initialised.
     */
    public function __construct()
    {
        $this->app = \App\Framework\Core\ApplicationContainer::$app;
    }
}
