<?php

namespace App\Framework\Core;

use App\Framework\Core\Interfaces\IApplication;

/**
 * The user's application.
 *
 * Provides a container and resolver, as well as some
 * methods for using the container and resolver.
 */
class Application implements IApplication
{
    /**
     * An instance of the DI container.
     *
     * @var \App\Framework\Core\Container\Container
     */
    private $_container;

    /**
     * An instance of the DI resolver.
     * @var \App\Framework\Core\Container\Resolver
     */
    private $_resolver;

    /**
     * Create a new application instance.
     *
     * Creates a new application instance, containing a new container and resolver.
     * Stores the app in the ApplicationContainer.
     */
    public function __construct()
    {
        $this->_container = new \App\Framework\Core\Container\Container;
        $this->_resolver = new \App\Framework\Core\Container\Resolver($this->_container);
        \App\Framework\Core\ApplicationContainer::SetApp($this);
    }

    /**
     * Creates a new class, with dependencies resolved from the container,
     * and injects it into the container.
     */
    public function new(string $className)
    {
        $class = $this->_resolver->new($className);

        return $this->_container->bind($class);
    }

    /**
     * Fetches a class from the DI container.
     */
    public function get(string $className)
    {
        return $this->_container->get($className);
    }

    /**
     * Calls a method on a class, with parameters resolved from the DI container.
     */
    public function callMethod(string $class, string $method): void
    {
        $this->_resolver->resolveMethod($class, $method);
    }
}
