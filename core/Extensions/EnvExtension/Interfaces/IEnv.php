<?php

namespace App\Framework\Extensions\EnvExtension;

interface IEnv
{
    public function get(string $key);
    public function exists(string $key);
}
