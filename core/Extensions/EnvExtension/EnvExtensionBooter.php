<?php

namespace App\Framework\Extensions\EnvExtension;

use App\Framework\Core\Extension;
use App\Framework\Extensions\EnvExtension\Env;

class EnvExtensionBooter extends Extension
{
    public $classes = [Env::class];
    public $dependencies = [];
    public $configs = [];

    public function boot()
    {
    }
}
