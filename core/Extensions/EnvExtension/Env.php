<?php

namespace App\Framework\Extensions\EnvExtension;

use App\Framework\Core\System\System;
use App\Framework\Extensions\EnvExtension\IEnv;

/**
 * Allows the retrieving of values from a .env file at the project root.
 */
class Env implements IEnv
{
    /**
     * An array of key => value pairs from the .env.
     *
     * @var array
     */
    private $_env;

    /**
     * Sets all variables in .env within the $env property.
     */
    public function __construct()
    {
        $path = System::GetRoot() . '/.env';
        $env_info = file($path);

        $keys = array_map(function ($line) {
            return explode('=', $line)[0];
        }, $env_info);

        $values = array_map(function ($line) {
            return explode('=', $line)[1];
        }, $env_info);

        $this->_env = array_combine($keys, $values);
    }

    /**
     * Gets a value from a key in $this->env.
     */
    public function get(string $key): string
    {
        $value = trim($this->_env[$key]);

        return $value;
    }

    /**
     * Checks if a value from a key exists in $this->env.
     */
    public function exists(string $key): bool
    {
        return isset($this->_env[$key]);
    }
}
