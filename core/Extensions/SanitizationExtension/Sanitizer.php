<?php

namespace App\Framework\Extensions\SanitizationExtension;

use App\Framework\Extensions\SanitizationExtension\ISanitizer;

class Sanitizer implements ISanitizer
{
    /**
     * A list of the sanitizers that can be used.
     *
     * Provides friendly names for the built in PHP sanitizers.
     *
     * @var array
     */
    private $_filters = [
        'email' => FILTER_SANITIZE_EMAIL,
        'encoded' => FILTER_SANITIZE_ENCODED,
        'quotes' => FILTER_SANITIZE_MAGIC_QUOTES,
        'float' => FILTER_SANITIZE_NUMBER_FLOAT,
        'int' => FILTER_SANITIZE_NUMBER_INT,
        'chars' => FILTER_SANITIZE_SPECIAL_CHARS,
        'tags' => FILTER_SANITIZE_STRING,
        'url' => FILTER_SANITIZE_URL
    ];

    /**
     * Sanitizes an array of data that is passed in.
     */
    public function sanitize(array $input): array
    {
        $rules = [];
        $newData = [];
        foreach ($input as $data => $rule) {
            $rules[$data] = explode('|', $rule);
        }
        foreach ($rules as $data => $sanitizations) {
            foreach ($sanitizations as $sanitization) {
                if ($sanitization === 'float') {
                    $data = filter_var($data, $this->_filters[$sanitization], FILTER_FLAG_ALLOW_FRACTION);
                } else {
                    $data = filter_var($data, $this->_filters[$sanitization]);
                }
            }
            array_push($newData, $data);
        }

        return $newData;
    }
}
