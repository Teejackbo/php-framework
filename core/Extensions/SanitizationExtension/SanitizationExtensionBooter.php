<?php

namespace App\Framework\Extensions\SanitizationExtension;

use App\Framework\Core\Extension;
use App\Framework\Extensions\SanitizationExtension\Sanitizer;

class SanitizationExtensionBooter extends Extension
{
    public $classes = [Sanitizer::class];
    public $dependencies = [];
    public $configs = [];

    public function boot()
    {
    }
}
