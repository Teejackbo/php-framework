<?php

namespace App\Framework\Extensions\SanitizationExtension;

interface ISanitizer
{
    public function sanitize(array $input);
}
