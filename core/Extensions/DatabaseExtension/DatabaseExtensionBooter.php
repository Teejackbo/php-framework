<?php

namespace App\Framework\Extensions\DatabaseExtension;

use App\Framework\Core\Extension;
use App\Framework\Extensions\DatabaseExtension\Connection;
use App\Framework\Extensions\DatabaseExtension\QueryBuilder;

class DatabaseExtensionBooter extends Extension
{
    public $classes = [Connection::class];
    public $dependencies = ['EnvExtension'];
    public $configs = [];

    public function boot(Connection $connection)
    {
        QueryBuilder::RegisterConnection(
            $connection
        );
    }
}
