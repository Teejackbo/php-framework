<?php

namespace App\Framework\Extensions\DatabaseExtension;

use App\Framework\Extensions\EnvExtension\Env;
use App\Framework\Extensions\DatabaseExtension\IConnection;

/**
 * Connects to a database.
 */
class Connection implements IConnection
{
    /**
     * The connection instance.
     *
     * @var \PDO
     */
    public $connection;

    /**
     * Connects to a database using information from .env
     */
    public function __construct(Env $env)
    {
        $this->connection = new \PDO(
            sprintf(
                '%s:host=%s;dbname=%s',
                $env->get('DATABASE_CONNECTION'),
                $env->get('DATABASE_HOST'),
                $env->get('DATABASE_NAME')
            ),
            $env->get('DATABASE_USERNAME'),
            $env->get('DATABASE_PASSWORD')
        );
    }

    /**
     * Prepares a string of SQL to be executed.
     */
    public function prepare(string $sql)
    {
        return $this->connection->prepare($sql);
    }
}
