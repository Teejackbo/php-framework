<?php

namespace App\Framework\Extensions\DatabaseExtension;

interface IModel
{
    public function save();
}
