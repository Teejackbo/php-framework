<?php

namespace App\Framework\Extensions\DatabaseExtension;

interface IQueryBuilder
{
    public function select(string ...$args);
    public function count(string $columnName);
    public function where(
        string $field = '',
        string $symbol = '=',
        string $value = ''
    );
    public function update(array $data);
    public function insert(array $data);
    public function fetch();
    public function first();
}
