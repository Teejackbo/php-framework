<?php

namespace App\Framework\Extensions\DatabaseExtension;

interface IConnection
{
    public function prepare(string $sql);
}
