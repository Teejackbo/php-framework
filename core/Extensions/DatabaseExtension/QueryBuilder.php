<?php

namespace App\Framework\Extensions\DatabaseExtension;

use App\Framework\Extensions\DatabaseExtension\Connection;
use App\Framework\Extensions\DatabaseExtension\IQueryBuilder;

/**
 * Provides an API for creating SQL queries.
 */
class QueryBuilder implements IQueryBuilder
{
    /**
     * The query that has been generated.
     *
     * @var string
     */
    private $_query;

    /**
     * A connection to the database.
     *
     * @var Connection
     */
    private static $_connection;

    /**
     * The table to perform the query on.
     *
     * @var string
     */
    private $_table;

    /**
     * The name of the model that is using the QueryBuilder.
     *
     * @var string
     */
    private $_model;

    /**
     * An array of all data that has been passed to the query.
     *
     * @var array
     */
    private $_inputs = [];

    /**
     * An array of symbols that are allowed in a where statement.
     *
     * @var array
     */
    private $_allowedSymbols = ['=', '<', '>', '<=', '>='];

    /**
     * Checks if a where statement is in the query.
     *
     * @var bool
     */
    private $_whereExecuted = false;

    /**
     * Checks if a count statement is in the query.
     *
     * @var bool
     */
    private $_countExecuted = false;

    /**
     * The field to count by.
     *
     * @var string
     */
    private $_countField = '';

    /**
     * Sets the table and model to use when running queries.
     */
    public function __construct(string $table, string $model)
    {
        $this->_table = $table;
        $this->_model = $model;
    }

    /**
     * Sets the connection to use when running queries.
     */
    public static function RegisterConnection(Connection $connection)
    {
        static::$_connection = $connection;
    }

    /**
     * Selects a number of fields from the database.
     */
    public function select(string ...$args): self
    {
        $field_list = '';
        if (count($args) == 1) {
            $field_list = $args[0];
        } else {
            $i = 0;
            foreach ($args as $arg) {
                // If it is the first iteration, do not add a comma at the start.
                if (!$i) {
                    $field_list .= $arg;
                } else {
                    $field_list .= ", $arg";
                }
                $i += 1;
            }
        }
        $this->_query .= "SELECT $field_list FROM `{$this->_table}`";

        return $this;
    }

    /**
     * Adds a count statement to the query.
     */
    public function count(string $columnName): self
    {
        $this->_query = "SELECT COUNT($columnName) FROM `{$this->_table}`";
        $this->_countExecuted = true;
        $this->_countField = $columnName;

        return $this;
    }

    public function where(
        string $field = '',
        string $symbol = '=',
        string $value = ''
    ): self {
        // If the symbol is not in allowed symbols, make that the value to run the where against.
        if (!in_array($symbol, $this->_allowedSymbols)) {
            $value = $symbol;
            $symbol = '=';
        }
        $prefix = 'WHERE';
        if ($this->_whereExecuted) {
            $prefix = 'AND';
        }
        $this->_query .= " $prefix `$field` $symbol '$value' ";
        $this->_whereExecuted = true;

        return $this;
    }

    /**
     * Updates a record in the database.
     */
    public function update(array $data)
    {
        $fields = '';
        $i = 0;
        foreach ($data as $field => $value) {
            if (!$i) {
                $fields .= "`$field` = :$field";
            } else {
                $fields .= ", `$field` = :$field";
            }
            $i += 1;
        }
        $this->_query = sprintf(
            'UPDATE %s SET %s WHERE id = :id',
            $this->_table,
            $fields
        );
        try {
            $statement = static::$_connection->prepare($this->_query);
            $statement->execute($data);
        } catch (Exception $e) {
            die('Something went wrong.');
        }
    }

    /**
     * Inserts a record in to the database.
     */
    public function insert(array $data)
    {
        $this->_query = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            $this->_table,
            implode(', ', array_keys($data)),
            ':' . implode(', :', array_keys($data))
        );
        try {
            $statement = static::$_connection->prepare($this->_query);
            $statement->execute($data);
        } catch (Exception $e) {
            die('Whoops, something went wrong.');
        }
    }

    /**
     * Returns all results from the query that was created.
     */
    public function fetch(): array
    {
        $statement = static::$_connection->prepare($this->_query);
        $statement->execute($this->_inputs);
        if ($this->_countExecuted) {
            return intval($statement->fetchAll(\PDO::FETCH_ASSOC)[0]["COUNT({$this->_countField})"]);
        }

        return $statement->fetchAll(\PDO::FETCH_CLASS, $this->_model);
    }

    /**
     * Returns only the first result from the created query.
     */
    public function first()
    {
        $this->_query .= ' LIMIT 1';
        $statement = static::$_connection->prepare($this->_query);
        $statement->execute($this->_inputs);
        if ($this->_countExecuted) {
            return intval($statement->fetchAll(\PDO::FETCH_ASSOC)[0]["COUNT({$this->_countField})"]);
        }

        return $statement->fetchAll(\PDO::FETCH_CLASS, $this->_model)[0];
    }
}
