<?php

namespace App\Framework\Extensions\DatabaseExtension;

use App\Framework\Extensions\DatabaseExtension\IModel;
use App\Framework\Extensions\DatabaseExtension\QueryBuilder;

/**
 * Provides mapping between the database and a resource.
 *
 * Exposes common methods for interacting with the database.
 */
class Model implements IModel
{
    /**
     * Properties which are not included as being a part of the information from/for the database.
     *
     * @var array
     */
    protected static $_ignoredProperties = ['table_name'];

    /**
     * Get's the table name of a model, for use in a query.
     */
    public static function GetTableName(): string
    {
        $calledClass = \get_called_class();
        $tableName = $calledClass::Table();

        if ($tableName) {
            return $tableName;
        }

        $class = strtolower($calledClass . 's');
        $classArray = explode('\\', $class);
        $generatedTableName = array_pop($classArray);

        return $generatedTableName;
    }

    /**
     * Provides a new instance of the QueryBuilder.
     */
    public static function Query(): QueryBuilder
    {
        $table = static::GetTableName();

        return new QueryBuilder($table, \get_called_class());
    }

    /**
     * Saves a model instance to the database.
     */
    public function save(): void
    {
        $props = static::GetProperties();
        $data = [];
        foreach ($props as $prop) {
            if (!in_array($prop, static::$_ignoredProperties)) {
                $data[$prop] = $this->$prop;
            }
        }
        $class = get_called_class();

        // If the class has an ID, update it, if not, insert it.
        if ($this->id) {
            $class::Query()->update($data);
        } else {
            $class::Query()->insert($data);
        }
    }

    /**
     * Returns the properties of a Model.
     */
    public static function GetProperties(): array
    {
        $class = get_called_class();
        $class = new \ReflectionClass("\\$class");
        $props = $class->getProperties();
        $ownProps = [];
        foreach ($props as $key => $prop) {
            if ($class->name == $prop->class) {
                array_push($ownProps, $prop->name);
            }
        }

        return $ownProps;
    }

    /**
     * Gets all instances from the database.
     */
    public static function All(): array
    {
        return get_called_class()::Query()
            ->select('*')
            ->fetch();
    }

    /**
     * Finds an instance from ID in the database.
     */
    public static function Find(int $id)
    {
        return get_called_class()::Query()
            ->select('*')
            ->where('id', $id)
            ->first();
    }

    /**
     * Finds all instances depending on a where condition.
     */
    public static function FindAllBy(string $field, $val, string $symbol = '='): array
    {
        return get_called_class()::Query()
            ->select('*')
            ->where($field, $symbol, $val)
            ->fetch();
    }
}
