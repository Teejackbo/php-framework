<?php

namespace App\Framework\Extensions\ServiceExtension;

use App\Config\ServicesConfig;
use App\Framework\Core\Extension;

class ServiceExtensionBooter extends Extension
{
    private $config;
    public $classes = [];
    public $dependencies = [];
    public $configs = [ServicesConfig::class];

    public function boot(ServicesConfig $config)
    {
        foreach($config->registeredServices as $service) {
            $this->app->new($service);
        }
    }
}
