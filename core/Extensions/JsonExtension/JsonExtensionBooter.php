<?php

namespace App\Framework\Extensions\JsonExtension;

use App\Framework\Core\Extension;
use App\Framework\Extensions\JsonExtension\Json;

class JsonExtensionBooter extends Extension
{
    public $classes = [Json::class];
    public $dependencies = [];
    public $configs = [];

    public function boot()
    {
    }
}
