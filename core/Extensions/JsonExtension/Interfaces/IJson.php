<?php

namespace App\Framework\Extensions\JsonExtensions;

interface IJson
{
    public function parse(string $json);
    public function compose(array $composeable);
}
