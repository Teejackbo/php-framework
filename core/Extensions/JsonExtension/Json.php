<?php

namespace App\Framework\Extensions\JsonExtension;

use App\Framework\Extensions\JsonExtensions\IJson;

/**
 * This class provides methods for parsing JSON.
 */
class Json implements IJson
{
    /**
     * Converts a string of JSON to a PHP equivalent.
     */
    public function parse(string $json)
    {
        return json_decode($json);
    }

    /**
     * Converts a variable to JSON.
     */
    public function compose(array $composeable)
    {
        return json_encode($composeable);
    }
}
