<?php

namespace App\Framework\Extensions\EventsExtension;

use App\Config\EventsConfig;
use App\Framework\Core\Extension;
use App\Framework\Extensions\EventsExtension\Event;

class EventsExtensionBooter extends Extension
{
    public $classes = [Event::class];
    public $dependencies = [];
    public $configs = [EventsConfig::class];

    public function boot()
    {
    }
}
