<?php

namespace App\Framework\Extensions\EventsExtension;

interface IEvent
{
    public function emit(string $event);
}
