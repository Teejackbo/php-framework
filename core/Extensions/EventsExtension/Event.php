<?php

namespace App\Framework\Extensions\EventsExtension;

use App\Config\EventsConfig;
use App\Framework\Core\ApplicationContainer;
use App\Framework\Extensions\EventsExtension\IEvent;

/**
 * Provides features for emitting and listening to events.
 */
class Event implements IEvent
{
    /**
     * The config option from App/config/EventsConfig.php
     *
     * @var EventsConfig
     */
    private $_config;

    /**
     * Register configuration.
     */
    public function __construct(EventsConfig $config)
    {
        $this->_config = $config;
    }

    /**
     * Emits an event.
     *
     * Calls the handle function for every event handler
     * registered to the emitted event.
     *
     * @throws \Exception if the event is not defined.
     */
    public function emit(string $event)
    {
        $flattenedTypes = $this->_flattenTypes($this->_config->types);
        $registered = in_array($event, $flattenedTypes);

        if ($registered) {
            $this->callListeners($event);
        } else {
            throw new \Exception("$event is not defined in EventsConfig.");
        }
    }

    /**
     * Calls listener methods for a specified event.
     */
    private function callListeners(string $event)
    {
        $listeners = $this->_config->listeners[$event] ?? [];

        foreach ($listeners as $listener) {
            $listenerInfo = explode('@', $listener);
            $listenerNamespace = "\\App\\Listeners\\$listenerInfo[0]";

            if (!method_exists($listenerNamespace, $listenerInfo[1])) {
                throw new \Exception("Method $listenerInfo[1] not defined on $listenerNamespace");
            }

            ApplicationContainer::$app->callMethod($listenerNamespace, $listenerInfo[1]);
        }
    }

    /**
     * Transforms associative arrays to a format of key:value
     *
     * Example: ['user' => ['loggedIn']] becomes ['user:loggedIn']
     */
    private function _flattenTypes($types, $parentKey = '')
    {
        $newTypes = [];

        foreach ($types as $type => $value) {
            if (is_array($value)) {
                $parent = $parentKey ? "$parentKey:$type" : $type;
                $flattenedTypes = $this->_flattenTypes($value, $parent);

                array_push($newTypes, ...$flattenedTypes);
            } else {
                $type = $parentKey ? "$parentKey:$value" : $value;
                $newTypes[] = $type;
            }
        }

        return $newTypes;
    }
}
