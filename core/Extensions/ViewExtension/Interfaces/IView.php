<?php

namespace App\Framework\Extensions\ViewExtension;

interface IView
{
    public function render($name = '', $data = []);
}
