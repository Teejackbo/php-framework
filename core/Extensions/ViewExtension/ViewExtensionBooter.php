<?php

namespace App\Framework\Extensions\ViewExtension;

use App\Framework\Core\Extension;
use App\Framework\Extensions\ViewExtension\View;

class ViewExtensionBooter extends Extension
{
    public $classes = [View::class];
    public $dependencies = [];
    public $configs = [];

    public function boot()
    {
    }
}
