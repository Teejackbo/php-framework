<?php

namespace App\Framework\Extensions\ViewExtension;

use App\Framework\Extensions\ViewExtension\IView;

class View implements IView
{
    /**
     * Renders a view, with data extracted from the second parameter.
     *
     * @throws \Exception if the requested view doesn't exist.
     */
    public function render($name = '', $data = [])
    {
        extract($data);
        if (!file_exists("App/Views/$name.view.php")) {
            throw new \Exception('Unable to find this view.');
        }
        require_once "App/Views/$name.view.php";
    }
}
