<?php

namespace App\Framework\Extensions\HttpExtension;

interface IRequest
{
    public function getMethod();
    public function isMethod(string $method);
    public function getUri();
}
