<?php

namespace App\Framework\Extensions\HttpExtension;

interface IRouter
{
    public function redirect();
}
