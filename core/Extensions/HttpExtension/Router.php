<?php

namespace App\Framework\Extensions\HttpExtension;

use App\Framework\Core\System\System;
use App\Framework\Core\ApplicationContainer;
use App\Framework\Extensions\ViewExtension\View;
use App\Framework\Extensions\HttpExtension\IRouter;
use App\Framework\Extensions\HttpExtension\Request;

/**
 * Handles the routing from registered routes to controllers.
 */
class Router implements IRouter
{
    /**
     * A mapping of registered routes.
     *
     * @var array
     */
    private $_routes = [
        'GET' => [],
        'POST' => [],
        'PUT' => [],
        'DELETE' => []
    ];

    /**
     * The current request object.
     *
     * @var Request
     */
    private $_request;

    /**
     * An instance of the view object.
     *
     * @var View
     */
    private $_view;

    public function __construct(Request $request, View $view)
    {
        $this->_request = $request;
        $this->_view = $view;
    }

    /**
     * Used to redirect a request to the correct controller.
     *
     * Loads a 404 page if the route is not mapped.
     */
    public function redirect()
    {
        $method = $this->_request->getMethod();
        $uri = $this->_request->getUri();
        $routeExists = array_key_exists($uri, $this->_routes[$method]);

        if ($routeExists) {
            $controller = $this->_routes[$method][$uri];
            $controller_info = explode('@', $controller->controller);
            $this->callMethod($controller_info[0], $controller_info[1]);
        } else {
            return $this->_view->render('404');
        }
    }

    /**
     * Calls the correct method on the correct controller.
     */
    private function callMethod(string $controller, string $method): void
    {
        $controller = "\\App\\Controllers\\{$controller}";
        ApplicationContainer::$app->callMethod($controller, $method);
    }

    /**
     * Registers a route within the internal mapping of routes.
     */
    public function registerRoute(Route $route): void
    {
        $this->_routes[$route->method][$route->name] = $route;
    }
}
