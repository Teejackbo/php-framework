<?php

namespace App\Framework\Extensions\HttpExtension;

use App\Framework\Extensions\HttpExtension\IRequest;

/**
 * Provides information about the incoming request.
 */
class Request implements IRequest
{
    /**
     * The HTTP method used.
     *
     * @var string
     */
    public $method;

    /**
     * The URI that was requested.
     *
     * @var string
     */
    public $uri;

    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->uri = $_SERVER['REQUEST_URI'];
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function isMethod(string $method): bool
    {
        return $this->getMethod() === $method;
    }

    public function getUri(): string
    {
        return $this->uri;
    }
}
