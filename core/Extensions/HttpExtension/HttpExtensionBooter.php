<?php

namespace App\Framework\Extensions\HttpExtension;

use App\Framework\Core\Extension;
use App\Framework\Core\System\System;
use App\Framework\Extensions\HttpExtension\Router;
use App\Framework\Extensions\HttpExtension\Request;

class HttpExtensionBooter extends Extension
{
    public $classes = [Request::class, Router::class];
    public $dependencies = ['ViewExtension'];
    public $configs = [];

    public function boot(Router $router)
    {
        require_once System::GetRoot() . '/App/config/routes.php';
        $router->redirect();
    }
}
