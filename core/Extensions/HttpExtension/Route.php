<?php

namespace App\Framework\Extensions\HttpExtension;

use App\Framework\Core\ApplicationContainer;
use App\Framework\Extensions\HttpExtension\IRoute;
use App\Framework\Extensions\HttpExtension\Router;

/**
 * Used for registering routes and mapping them to a controller.
 */
class Route implements IRoute
{
    /**
     * The URI that the route is mapped to.
     *
     * @var string
     */
    public $uri;

    /**
     * The HTTP method that the route corresponds to.
     *
     * @var string
     */
    public $method;

    /**
     * The controller method to route the request to.
     *
     * Example: PostController@index
     *
     * @var string
     */
    public $controller;

    public function __construct(string $uri, string $method, string $controller)
    {
        $this->name = $uri;
        $this->method = $method;
        $this->controller = $controller;
    }

    /**
     * Registers the route within the router.
     */
    private static function MakeRoute(string $uri, string $method, string $controller): self
    {
        if ($uri[0] != '/') {
            $uri = "/{$uri}";
        }
        $route = new static($uri, $method, $controller);
        $router = ApplicationContainer::$app->get(Router::class);
        $router->registerRoute($route);

        return $route;
    }

    /**
     * Registers a GET route.
     */
    public static function Get(string $uri, string $controller)
    {
        return static::MakeRoute($uri, 'GET', $controller);
    }

    /**
     * Registers a POST route.
     */
    public static function Post(string $uri, string $controller)
    {
        return static::MakeRoute($uri, 'POST', $controller);
    }

    /**
     * Registers a PUT route.
     */
    public static function Put(string $uri, string $controller)
    {
        return static::MakeRoute($uri, 'PUT', $controller);
    }

    /**
     * Registers a DELETE route.
     */
    public static function Delete(string $uri, string $controller)
    {
        return static::MakeRoute($uri, 'DELETE', $controller);
    }
}
