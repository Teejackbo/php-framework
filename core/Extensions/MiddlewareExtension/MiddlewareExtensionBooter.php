<?php

namespace App\Framework\Extensions\MiddlewareExtension;

use App\Config\MiddlewareConfig;
use App\Framework\Core\Extension;

/**
 * Handles the running of global middleware.
 */
class MiddlewareExtensionBooter extends Extension
{
    public $classes = [];
    public $dependencies = [];
    public $configs = [MiddlewareConfig::class];

    /**
     * Runs all of the global middleware in the sequence defined in App/config/HttpConfig
     */
    public function boot(MiddlewareConfig $config)
    {
        foreach ($config->globalMiddleware as $middleware) {
            if (!method_exists($middleware, 'handle')) {
                throw new \Exception("No handle method defined on $middleware");
            }
            $this->app->callMethod($middleware, 'handle');
        }
    }
}
