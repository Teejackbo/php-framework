<?php

namespace App\Framework\Extensions\SessionExtension;

use App\Framework\Extensions\SessionExtension\ISession;

/**
 * Provides session management.
 */
class Session implements ISession
{
    /**
     * Starts a session.
     */
    public function __construct()
    {
        session_start();
    }

    /**
     * Sets an item in the session.
     */
    public function set(string $key, $val)
    {
        $_SESSION[$key] = $val;
    }

    /**
     * Gets an item from the session.
     *
     * @throws \Exception if the item doesn't exist in the session.
     */
    public function get(string $key)
    {
        if (array_key_exists($key, $_SESSION)) {
            return $_SESSION[$key];
        }
        throw new \Exception('Session Key not found.');
    }

    /**
     * Removes an item from the session.
     *
     * @throws \Exception if the item doesn't exist in the session.
     */
    public function delete(string $key)
    {
        if (array_key_exists($key, $_SESSION)) {
            unset($_SESSION[$key]);
        } else {
            throw new \Exception('Session key not found.');
        }
    }

    /**
     * Clears the session.
     */
    public function clear()
    {
        session_unset();
    }

    /**
     * Deletes the session.
     */
    public function destroy()
    {
        session_destroy();
    }
}
