<?php

namespace App\Framework\Extensions\SessionExtension;

interface ISession
{
    public function set(string $key, $val);
    public function get(string $key);
    public function delete(string $key);
    public function clear();
    public function destroy();
}
