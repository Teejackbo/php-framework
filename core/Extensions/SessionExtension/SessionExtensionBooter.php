<?php

namespace App\Framework\Extensions\SessionExtension;

use App\Framework\Core\Extension;
use App\Framework\Extensions\SessionExtension\Session;

class SessionExtensionBooter extends Extension
{
    public $classes = [Session::class];
    public $dependencies = [];
    public $configs = [];

    public function boot()
    {
    }
}
