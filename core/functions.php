<?php

/**
 * Stops execution and dumps the variable that is passed to it.
 */
function dd($var)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
    die();
}
