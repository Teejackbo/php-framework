<?php

namespace App\Framework\Core\Interfaces;

interface IResolver
{
    public function new(string $className);
    public function resolveMethod(string $className, string $methodName);
}
