<?php

namespace App\Framework\Core\Interfaces;

interface IContainer
{
    public function bind($class);
    public function get($class);
}
