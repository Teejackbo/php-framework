<?php

namespace App\Framework\Core\Interfaces;

interface IApplication
{
    public function new(string $className);
    public function get(string $className);
    public function callMethod(string $class, string $method);
}
