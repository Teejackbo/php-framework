<?php

namespace App\Framework\Core\Interfaces;

interface IExtensionBooter
{
    public function bootExtensions();
}
