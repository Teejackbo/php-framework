<?php

namespace App\Framework\Core\Container;

use ReflectionClass;
use ReflectionMethod;
use App\Framework\Core\Container\Container;
use App\Framework\Core\Interfaces\IResolver;

/**
 * Injects dependencies from the DI container in to a class or method.
 */
class Resolver implements IResolver
{
    /**
     * An instance of a container. Used to resolve dependencies.
     *
     * @var Container
     */
    private $_container;

    /**
     * Creates a new resolver.
     *
     * @param Container $container An instance of a container, to resolve dependencies from.
     */
    public function __construct(Container $container)
    {
        $this->_container = $container;
    }

    /**
     * Returns an instance of a class, with dependencies injected into its constructor.
     */
    public function new(string $className)
    {
        // Create a new ReflectionClass, to get information about the properties of the class.
        $reflector = new ReflectionClass($className);
        $constructor = $reflector->getConstructor();

        // If there are no dependencies, return a new instance of the class.
        if (is_null($constructor)) {
            return $reflector->newInstance();
        }

        $params = $constructor->getParameters();
        $dependencies = $this->_getDependencies($params);

        // Returns a new instance of the class, with dependencies resolved.
        return $reflector->newInstanceArgs($dependencies);
    }

    /**
     * Creates a new instance of a class, and calls a method on it.
     *
     * Resolves dependencies in the constructor and method itself.
     */
    public function resolveMethod(string $className, string $methodName)
    {
        // Get a new instance of the class, with constructor dependencies resolved.
        $class = $this->new($className);

        // Create a new ReflectionMethod to get information about the arguments
        // of the method.
        $reflector = new ReflectionMethod($className, $methodName);
        $params = $reflector->getParameters();
        $dependencies = $this->_getDependencies($params);

        // If the method is static, call it without an instance of a class.
        if ($reflector->isStatic()) {
            return $reflector->invokeArgs(null, $dependencies);
        }

        // Call the method with arguments resolved.
        return $reflector->invokeArgs($class, $dependencies);
    }

    /**
     * Creates an array of dependencies from an array of parameters.
     *
     * @throws Exception if there is no default value available, and no class specified.
     */
    private function _getDependencies(array $params): array
    {
        $dependencies = [];
        foreach ($params as $param) {
            $dependency = $param->getClass();

            if ($dependency === null) {
                if ($param->isDefaultValueAvailable()) {
                    $dependencies[] = $param->getDefaultValue();
                } else {
                    throw new \Exception("Can not resolve class dependency {$param->name}");
                }
            } else {
                // Retrieve the dependency from the container.
                $dependencies[] = $this->_container->get($dependency->name);
            }
        }

        return $dependencies;
    }
}
