<?php

namespace App\Framework\Core\Container;

use App\Framework\Core\Interfaces\IContainer;

/**
 * The dependency injection container.
 *
 * Used for resolving dependencies throughout the application.
 */
class Container implements IContainer
{
    /**
     * A store of all of the injected dependencies.
     *
     * @var array
     */
    private $_registry = [];

    /**
     * Stores a class in the registry.
     */
    public function bind($class)
    {
        $this->_registry[get_class($class)] = $class;

        return $this->_registry[get_class($class)];
    }

    /**
     * Retrieves a class from the registry.
     *
     * @throws Exception if the class cannot be found in the registry.
     */
    public function get($class)
    {
        $dependency = $this->_registry[$class];
        if (isset($dependency)) {
            return $dependency;
        }
        throw new \Exception("Could not find $class in the container");
    }
}
