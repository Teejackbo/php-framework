<?php

namespace App\Framework\Core;

use App\Framework\Core\Interfaces\IApplication;
use App\Framework\Core\Interfaces\IApplicationContainer;

/**
 * Provides a static place to store the application instance, for access
 * in other methods where DI is impossible.
 */
class ApplicationContainer implements IApplicationContainer
{
    /**
     * The application instance.
     *
     * @var IApplication
     */
    public static $app;

    /**
     * Sets the $app property.
     */
    public static function SetApp(IApplication $app)
    {
        static::$app = $app;
    }
}
