<?php

namespace App\Framework\Core;

use App\Config\ExtensionsConfig;
use App\Framework\Core\Extension;
use App\Framework\Core\Interfaces\IExtensionBooter;

/**
 * Handles booting extensions that are registered for use.
 */
class ExtensionBooter implements IExtensionBooter
{
    /**
     * The created application.
     *
     * @var \App\Framework\Core\Application
     */
    private $_app;

    /**
     * The configuration class defined in App\Config\ExtensionsConfig.php
     * 
     * @var ExtensionsConfig
     */
    private $_config;

    /**
     * Loads the app as a property, and gathers the extensions to be booted.
     */
    public function __construct(ExtensionsConfig $config)
    {
        $this->_app = \App\Framework\Core\ApplicationContainer::$app;
        $this->_config = $config;
    }

    /**
     * Determines if the extensions dependencies are met.
     */
    private function _checkDependencies(Extension $extension): void
    {
        $dependenciesMet = true;
        $registeredExtensions = $this->_config->booters;

        // Get the extensions dependencies and name.
        $dependencies = $extension->dependencies;
        $extensionName = get_class($extension);

        // Each dependency of the extension.
        foreach($dependencies as $dependency) {
            $dependencyMet = false;

            // Each registered extension.
            foreach ($registeredExtensions as $extension) {

                // If this extension meets the dependency.
                $correctExtension = strpos($extension, $dependency) === false ? false : true;
                if ($dependencyMet === false) {
                    $dependencyMet = $correctExtension;
                }
            }

            if ($dependencyMet === false) {
                throw new \Exception("$extensionName relies on $dependency, but it could not be found.");
            }
        }
    }

    /**
     * Runs through $this->extensions and first injects their registered $classes,
     * and then runs the boot method on each extension.
     */
    public function bootExtensions(): void
    {
        $booters = $this->_config->booters;
        
        // Injects each extensions classes into the DI container.
        foreach ($booters as $booter) {
            $booter = $this->_app->new($booter);
            $this->_checkDependencies($booter);
            if ($booter->configs) {
                foreach($booter->configs as $config) {
                    $this->_app->new($config);
                }
            }
            if ($booter->classes) {
                foreach ($booter->classes as $class) {
                    $this->_app->new($class);
                }
            }
        }

        // Runs the boot method on each extension, with dependency injection from the container.
        foreach ($booters as $booter) {
            if (!method_exists($booter, 'boot')) {
                throw new \Exception("No boot method specified on $booter");
            }
            $this->_app->callMethod($booter, 'boot');
        }
    }
}
