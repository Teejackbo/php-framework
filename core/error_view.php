<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $message ?></title>
</head>
<body style="display: flex; align-items: center; justify-content: center; background: #eeeeee; width: 100vw; height: 100vh; margin: 0;">
    <div style="background: #f7f7f7; box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.2); font-family: sans-serif; color: #3d3e3f; padding: 1em;">
        <div style="background: #f5f5f5;"><?= $message ?></div>
        <hr>
        <div style="background: #f5f5f5;">At line <?= $line ?> in <?= $file[count($file) - 1] ?></div>
        <hr>
        <?php foreach ($trace as $item) : ?>
            <?php if (isset($item['file'])) : ?>
                <div style="margin: 1em 0;">
                    <?php $exploded = explode('/', $item['file']) ?>
                    at <?=
                        (isset($item['class']) ? $item['class'] : '') .
                        (isset($item['type']) ? $item['type'] : '') .
                        (isset($item['function']) ? $item['function'] : '')
                        ?>
                    <div>
                        in <?= end($exploded) ?> on line <?= $item['line'] ?>
                    </div>
                </div>
            <?php endif ?>
        <?php endforeach; ?>
    </div>
</body>
</html>
